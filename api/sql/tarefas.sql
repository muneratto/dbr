/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : app_gerenciador

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-02-18 17:32:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tarefas`
-- ----------------------------
DROP TABLE IF EXISTS `tarefas`;
CREATE TABLE `tarefas` (
  `cdTarefa` int(11) NOT NULL AUTO_INCREMENT,
  `stTitulo` varchar(245) DEFAULT NULL,
  `stDescricao` text,
  `stPrioridade` char(1) DEFAULT NULL,
  `stPos` int(11) DEFAULT NULL,
  PRIMARY KEY (`cdTarefa`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tarefas
-- ----------------------------
INSERT INTO `tarefas` VALUES ('9', 'teste 001', 'teste 001', '0', '1');
INSERT INTO `tarefas` VALUES ('10', 'teste 001', 'teste 001', '0', '2');
INSERT INTO `tarefas` VALUES ('11', 'teste 001', 'teste 001', '0', '0');
INSERT INTO `tarefas` VALUES ('12', 'teste 001', 'teste 001', '0', '4');
INSERT INTO `tarefas` VALUES ('13', 'teste 001', 'teste 001', '1', '1');
INSERT INTO `tarefas` VALUES ('14', 'teste 001', 'teste 001', '1', '0');
INSERT INTO `tarefas` VALUES ('15', 'teste 001', 'teste 001', '1', '2');
INSERT INTO `tarefas` VALUES ('16', 'teste 001', 'teste 001', '1', '3');
INSERT INTO `tarefas` VALUES ('17', 'teste 001sdfsdfs', 'teste 001 sdfsfs', '2', '2');
INSERT INTO `tarefas` VALUES ('18', 'teste 001', 'teste 001', '2', '3');
INSERT INTO `tarefas` VALUES ('19', 'teste 001', 'teste 001', '2', '1');
INSERT INTO `tarefas` VALUES ('20', 'TESTE DO DIOGO', 'DIOGO', '2', '0');
INSERT INTO `tarefas` VALUES ('21', 'SDFS', 'SDFSDF', '0', '3');

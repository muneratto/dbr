<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Cache\Cache;

class TarefasController extends AppController{

    public function initialize(){
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function index(){

        #metodo get
        #/tarefas/index
        #/tarefas/index.json
        #/tarefas/index.xml

        $this->set("title", "Tarefas");

        $tarefas = $this->Tarefas->find('all', array('order'=>array('Tarefas.stPos'=>'ASC', 'Tarefas.cdTarefa'=>'DESC')));
        $this->set('tarefas', $tarefas);

        $this->set([
                    'tarefas'=>$tarefas,
                    '_serialize'=>['tarefas']
                    ]);

        $this->viewBuilder()->setLayout('layout');
    }


    public function view($id){

        #metodo get
        #/tarefas/view/5
        #/tarefas/view/5.json
        #/tarefas/view/5.xml

        $tarefa = $this->Tarefas->get($id);
        $this->set([
            'tarefa'=>$tarefa,
            '_serialize'=>['tarefa']
        ]);

        $this->viewBuilder()->setLayout('layout');
    }

    public function add(){
        #metodo post
        #campo['stTitulo', 'stDescricacao', 'stPrioridade']

        $tarefa = $this->Tarefas->newEntity();
        if ($this->request->is('post')) {

            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->data);

            if ($this->Tarefas->save($tarefa)) {
                $message = 'msg:Cadastro efetuado com sucesso.';
            } else {
                $message = 'msg:Ops não foi salvo!';
            }
        }

        $this->viewBuilder()->setLayout('layout');
    }

    public function sortable(){
        if(isset($_POST['arrayorder'])){
            $tarefa = $this->Tarefas->newEntity();
            foreach ($_POST['arrayorder']as $key => $value) {
                $array = array('cdTarefa' => $value, 'stPos' => $key);
                
                $tarefa = $this->Tarefas->patchEntity($tarefa, $array);
                $this->Tarefas->save($tarefa);
            }
        }
        exit();
    }

    public function edit($id = null){

        #metodo post/put
        #/tarefas/edit/5
        #/tarefas/edit/5.json
        #/tarefas/edit/5.xml
        #campo['stTitulo', 'stDescricacao', 'stPrioridade']

        $this->set("title", "Tarefa");

        $tarefa = $this->Tarefas->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Tarefas->patchEntity($tarefa, $this->request->data);
            if ($this->Tarefas->save($tarefa)) {
                $message = "msg:Cadastro atualizado com sucesso.";
            }else{
                $message = 'msg:Ops não foi atualizado!';
            }
        }

        $this->set('tarefa', $tarefa);

        $this->viewBuilder()->setLayout('layout');
    }

    public function delete($id){

        #metodo post/delet
        #/tarefas/delete/5
        #/tarefas/delete/5.json
        #/tarefas/delete/5.xml

        $this->request->allowMethod(['post', 'delete']);
        $tarefa = $this->Tarefas->get($id);
        if ($this->Tarefas->delete($tarefa)) {
            $message = "msg:Cadastro deletado com sucesso.";
        }else{
            $message = 'msg:Ops não foi deletado!';
        }

        $this->viewBuilder()->setLayout('layout');
    }
}

?>

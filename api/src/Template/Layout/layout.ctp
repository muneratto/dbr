<?php
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Diogo Muneratto"> 
    
    <title>

    </title>





    <?= $this->Html->css('font-awesome.min.css') ?>
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('jquery-confirm.css') ?>
    <?= $this->Html->css('default.css') ?>

    <?= $this->Html->script('jquery.min'); ?>
    <?= $this->Html->script('jquery-ui.min'); ?>
    <?= $this->Html->script('bootstrap.min'); ?>
    <?= $this->Html->script('bootstrap.min'); ?>
    <?= $this->Html->script('jquery-confirm'); ?>
    <?= $this->Html->script('script'); ?>
  


</head>
<body>
    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>
</body>
</html>

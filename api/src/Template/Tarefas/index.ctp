    <div class="container">
        
        <header>
            <div class="title">
                <h1 class='pull-left'>Gerenciador de Tarefas</h1>
                <a href='javascript:;' data='' class='btn btn-success edit pull-right'><i class="fa fa-plus" aria-hidden="true"></i> Novo</a>
            </div>
            <div class='cls'></div>
        </header>
        <hr/>
        <div class="row">
            <?php

                $a = array();
                foreach ($tarefas as $key => $value) {
                    $a[$value['stPrioridade']][$key] = $value;
                }

                foreach ($a as $k => $v) {

                    switch ($k) {
                        case 0:
                            $prioridade = 'Baixa';
                            $cssPanel = 'list-group-item-success';
                            break;
                        case 1:
                            $prioridade = 'Média';
                            $cssPanel = 'list-group-item-warning';
                            break;
                        case 2:
                            $prioridade = 'Alta';
                            $cssPanel = 'list-group-item-danger';
                            break;
                    } 

                    echo "<ul class='list-items ui-sortable col-md-4'>";
                        echo '<h2>'.$prioridade.'</h2>';
                        foreach ($v as $key => $tarefa) {

                            echo "<li id='arrayorder_{$tarefa['cdTarefa']}'  class='item {$cssPanel}'>
                                    <div class='item-title'>{$tarefa['stTitulo']}</div>
                                    <div class='item-description'>{$tarefa['stDescricao']}</div>
                                    <p>Prioridade: <b>{$prioridade}</b></p>
                                    <label class='checkbox-inline check'><input type='checkbox'></label>
                                    <div class='drag-handler'></div>
                                    <div class='bt'>
                                        <a href='javascript:;' data='{$tarefa['cdTarefa']}' class='btn btn-info edit'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                                        <a href='javascript:;' data='{$tarefa['cdTarefa']}' class='btn btn-danger del'><i class='fa fa-times' aria-hidden='true'></i></a>
                                    </div>
                                  </li>";
                        }
                    echo "</ul>";
                }

                ?>
            </ul>
        </div>
</div>



<script type="text/javascript">
    
    var uri =  '/bdr/api/tarefas/';

    $('.del').click(function(){
        var _this = $(this);
        var id = _this.attr('data');

        $.confirm({
            title: 'Atenção!',
            content: 'Tem certeza que deseja excluir este registro',
            buttons: {
                confirm: function () {
                    _this.parent().parent().remove();
                    $.post(uri+'/delete/'+id+'.json', function(data){
                    })
                },
                cancel: function(){

                }
            }
        });
    })

    $('.edit').click(function(){

        var _this = $(this);
        var id = _this.attr('data');
        var url = uri+'add';
        var urlSend = uri+'add.json';

        if(id != ''){
            url = uri+'edit/'+id;
            urlSend = uri+'edit/'+id+'.json';
        }
       
        $.confirm({
            title: 'Atenção!',
            theme: 'newmodal',
            content: 'url:'+url,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            buttons: {
                confirm: function () {
                    var dados = $('#modal').serialize();
                    $.post(urlSend, dados, function(data){
                        location.reload();
                    })
                },
                cancel: function(){

                }
            }
        });
    })



    $(".ui-sortable").sortable({ opacity: 0.8, cursor: 'move', update: function() {
        var order = $(this).sortable("serialize") + '&update=update'; 
        $.post(uri+"sortable", order, function(theResponse){
            console.log(theResponse)
        });                                                          
    }});



</script>
<form id="modal" class='form' method="POST" enctype="multipart/form-data" action="" onsubmit="return false;">
    <div class="form-group">
      <label>Titulo</label>
      <input name="stTitulo" type="text" class="form-control col-md-12" placeholder="Titulo">
    </div>
    <div class="form-group">
      <label>Descrição</label>
      <textarea name="stDescricao" class="form-control col-md-12"></textarea>
    </div>
    <div class="form-group">
      <label>Prioridade</label>
      <select name="stPrioridade" class="form-control col-md-12">
          <option value="0">Baixa</option>
          <option value="1">Média</option>
          <option value="2">Alta</option>
      </select>
    </div>
    <div class='clear'></div>
</form>
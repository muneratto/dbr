<form id="modal" class='form' method="POST" enctype="multipart/form-data" action="" onsubmit="return false;">
    <div class="form-group">
      <label>Titulo</label>
      <input name="stTitulo" type="text" class="form-control col-md-12" placeholder="Titulo" value="<?php echo $tarefa['stTitulo']; ?>">
    </div>
    <div class="form-group">
      <label>Descrição</label>
      <textarea name="stDescricao" class="form-control  col-md-12"><?php echo $tarefa['stDescricao']; ?></textarea>
    </div>
    <div class="form-group">
      <label>Prioridade</label>
      <select name="stPrioridade" class="form-control col-md-12">
          <option value="0" <?php echo ($tarefa['stPrioridade'] == 0)? 'selected' : '' ; ?>>Baixa</option>
          <option value="1" <?php echo ($tarefa['stPrioridade'] == 1)? 'selected' : '' ; ?>>Média</option>
          <option value="2" <?php echo ($tarefa['stPrioridade'] == 2)? 'selected' : '' ; ?>>Alta</option>
      </select>
    </div>
    <div class='clear'></div>
</form>
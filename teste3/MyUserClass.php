<?php

	//require 'DatabaseConnection.php';

	class DatabaseConnection {

		public $conn;

        function DatabaseConnection($host, $user, $pass) {

            $conn = mysqli_connect($host,$user,$pass,'app_gerenciador');

            if (!$conn){
  				die("Connection error: " . mysqli_connect_error());
  			}

  			$this->conn = $conn;
  			return $conn;
        }

        function query($sql){

        	$conn = $this->conn;

        	$rs = $conn->query($sql);
            while ($row = $rs->fetch_object()){
		        $results[] = $row;
			}

            return $results;
        }

    }


	class MyUserClass {

		public function getUserList(){
			$dbconn = new DatabaseConnection('localhost','root','');
			$results = $dbconn->query('select stTitulo from tarefas');
			sort($results);
			return $results;

		}
	}

	
	$obj = new MyUserClass();
	$rs = $obj->getUserList();
	print_r($rs);

?>